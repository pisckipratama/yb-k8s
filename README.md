## Install YugaByte in Kubernetes cluster

1. access to jumphost TKGI INSW
```bash
ssh root@10.239.9.18
```

2. run script *tkgi-prod-large.sh*
```bash
sh ~/tkgi-prod-large.sh
```

3. add helm repo yugabyte
```bash
helm repo add yugabytedb https://charts.yugabyte.com
```

4. update helm repo
```bash
helm repo update
```

5. check helm chart yugabyte is existing
```bash
helm search repo yugabytedb/yugabyte --version 2.14
```

6. install yugabyte db single node sebagai testing di namespace yb-single
```bash
helm install yb-single yugabytedb/yugabyte  \ 
  --version 2.14.5 \
  --set replicas.master=1,replicas.tserver=1,enableLoadBalancer=true,storage.master.storageClass=managed-nfs-storage,storage.tserver.storageClass=managed-nfs-storage \
  --namespace yb-single \
  --create-namespace
```

7. check yugabyte's pods, services, and pvc
```bash
kubectl get pod,svc,pvc -n yb-single
```
